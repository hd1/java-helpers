<!DOCTYPE xsl>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns="http://www.canto.com/ns/Export/1.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="text" indent="yes" encoding="utf-8"
		standalone="yes" />
	<xsl:strip-space elements="*" />

	<!-- Parameterized separator/end of line characters for flexibility -->
	<xsl:param name="sep" select="'&#09;'" />
	<xsl:param name="eol" select="'&#10;'" />
	<xsl:param name="listsep" select="'|'" />

	<!-- On matching the root node, output a list of field names, followed by 
		the items -->
	<xsl:template match="/qna/results">
		<xsl:apply-templates select="/qna/results/result" />
	</xsl:template>

	<!-- On matching all but the last field name, output the name followed by 
		separator -->
	<xsl:template match="qna/results/result[position()!=last()]">
		<xsl:value-of select="concat(normalize-space(ns:Name),$sep)" />
	</xsl:template>

	<!-- On matching the last field name, output the name followed by a newline -->
	<xsl:template match="qna/results/result[position()=last()]">
		<xsl:value-of select="concat(normalize-space(ns:Name),$eol)" />
	</xsl:template>

	<!-- On matching an item, iterate through each field, applying templates 
		to any 'ns:FieldValue' nodes that share the same value of @uid -->
	<xsl:template match="qna/results/result">
		<xsl:variable name="item" select="*" />
		<xsl:for-each select="$item">
			<xsl:if test="position()!=last()">
				<xsl:value-of select="$sep" />
			</xsl:if>
		</xsl:for-each>
		<xsl:value-of select="$eol" />
	</xsl:template>

	<!-- On matching a field value, output the content. -->
	<xsl:template match="$item">
		<xsl:value-of select="normalize-space(.)" />
	</xsl:template>

</xsl:stylesheet>