/**
 * 
 */
package info.hasandiwan.java;

import java.beans.XMLDecoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Triggers updates (SVNPostCommit#postCommitHook) on a request
 * 
 * @author hdiwan
 * 
 */
public class SVNServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger("SVNServlet");
	static HashMap<URL, File> remoteToLocal = null;

	@Override
	public void init() {
		refresh();
	}
	
	@SuppressWarnings("unchecked")
	private static void refresh() {
		XMLDecoder xmlDecoder;
		try {
			xmlDecoder = new XMLDecoder(new FileInputStream(
					System.getProperty("java.io.tmpdir")));
			remoteToLocal = (HashMap<URL, File>) xmlDecoder.readObject();
		} catch (FileNotFoundException e) {
			// first run
			remoteToLocal = new HashMap<URL, File>();
		}
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		URL repositoryUrl = new URL(request.getParameter("repoURL"));
		if (remoteToLocal.keySet().contains(repositoryUrl)) {
			logger.warning(repositoryUrl.toExternalForm()+" found and updating!");
			SVNPostCommit postCommit = new SVNPostCommit();
			postCommit.postCommitHook(remoteToLocal.get(repositoryUrl).getAbsolutePath(), request.getParameter("repoURL"));
			
		}
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		remoteToLocal.put(new URL(request.getParameter("remote")), new File(request.getParameter("local")));
	}
}
