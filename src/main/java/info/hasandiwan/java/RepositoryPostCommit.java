/**
 * 
 */
package info.hasandiwan.java;
/**
 * @author hdiwan
 *
 */
public interface RepositoryPostCommit {

	/**
	 * @param path
	 * @param repositoryURL
	 */
	public void postCommitHook(String path, String repositoryURL);

}
