/**
 * 
 */
package info.hasandiwan.java;

import java.io.File;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.joda.time.DateTime;

/**
 * @author hdiwan
 * 
 */
public class TransformXML {
	public static void myTransformer(String sourceID, String xslID) {

		// Create a transform factory instance.
		TransformerFactory tfactory = TransformerFactory.newInstance();

		// Create a transformer for the stylesheet.
		Transformer transformer = null;
		try {
			transformer = tfactory.newTransformer(new StreamSource(new File(
					xslID)));
		} catch (TransformerConfigurationException e) {
			TransformXML.handleException(e);
		}

		// Transform the source XML to System.out.
		try {
			transformer.transform(new StreamSource(new File(sourceID)),
					new StreamResult(System.out));
		} catch (TransformerException e) {
			TransformXML.handleException(e);
		}
	}

	private static void handleException(Exception ex) {
		System.err.println(new DateTime()+":EXCEPTION: " + ex.getLocalizedMessage());
		ex.printStackTrace();
	}

	public static void transform(String foo_xml, String foo_xsl) {

		System.setProperty("javax.xml.transform.TransformerFactory",
				"net.sf.saxon.TransformerFactoryImpl");
		myTransformer(foo_xml, foo_xsl);

	}
}
