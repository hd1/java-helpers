/**
 * 
 */
package info.hasandiwan.java;

import java.io.File;
import java.util.logging.Logger;

import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;

/**
 * @author hdiwan
 * 
 */
public class SVNPostCommit implements RepositoryPostCommit {
	private static Logger logger = Logger.getLogger("SVNPostCommit");
	
	public void postCommitHook(String path, String repositoryURL) {
		File dstPath = new File(path);
		SVNClientManager cm = SVNClientManager.newInstance();
		SVNUpdateClient uc = cm.getUpdateClient();
		try {
			uc.doUpdate(dstPath, SVNRevision.HEAD,SVNDepth.INFINITY,true, true);
		} catch (SVNException e) {
			logger.throwing("SVNPostCommit", "postCommitHook", e);
			logger.warning(e.getLocalizedMessage());
			e.printStackTrace(System.err);
		}
	}
}
