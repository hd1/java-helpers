/**
 * 
 */
package info.hasandiwan.java;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class JDBC2XML {
	public static String go() {
		String ret = null;
		StringWriter ret_ = new StringWriter();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(System.getProperty("jdbc.url"));
		} catch (SQLException e1) {
			e1.printStackTrace(System.err);
			System.exit(-1);
		}
		Statement statement = null;
		try {
			statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e1) {
			e1.printStackTrace(System.err);
			System.exit(-1);
		}
		long start = System.currentTimeMillis();
		ResultSet results = null;
		try {
			results = statement.executeQuery(System.getProperty("jdbc.query"));
		} catch (SQLException e1) {
			e1.printStackTrace(System.err);
			System.exit(-1);
		}
		long runtime = System.currentTimeMillis() - start;
		ResultSetMetaData metadata = null;
		try {
			metadata = results.getMetaData();
		} catch (SQLException e1) {
			e1.printStackTrace(System.err);
		}
		SAXTransformerFactory factory = (SAXTransformerFactory) TransformerFactory
				.newInstance();
		TransformerHandler th = null;
		try {
			th = factory.newTransformerHandler();
		} catch (TransformerConfigurationException e1) {
			e1.printStackTrace(System.err);
		}
		th.setResult(new StreamResult(ret_));
		try {
			th.startDocument();
		} catch (SAXException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			th.startElement(null, null, "qna", null);
		} catch (SAXException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			th.startElement(null, null, "query", null);
		} catch (SAXException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			th.characters(System.getProperty("jdbc.query").toCharArray(), 0,
					System.getProperty("jdbc.query").length());
		} catch (SAXException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			th.endElement(null, null, "query");
		} catch (SAXException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			th.startElement(null, null, "runtime", null);
		} catch (SAXException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			th.characters(new Long(runtime).toString().toCharArray(), 0,
					new Long(runtime).toString().length());
		} catch (SAXException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			th.endElement(null, null, "runtime");
		} catch (SAXException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			th.startElement(null, null, "results", null);
		} catch (SAXException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			results.first();
			for (results.next(); results.isAfterLast() == false; results.next()) {
				
				try {
					th.startElement(null, null, "result", null);
				} catch (SAXException e1) {
					e1.printStackTrace(System.err);
				}
				try {
					th.startElement(null, null, "sequence", null);
				} catch (SAXException e1) {
					e1.printStackTrace(System.err);
				}
				try {
					th.characters(new Integer(results.getRow()).toString()
							.toCharArray(), 0, new Integer(results.getRow())
							.toString().length());
				} catch (SAXException e1) {
					e1.printStackTrace(System.err);
				} catch (SQLException e1) {
					e1.printStackTrace(System.err);
				}
				try {
					th.endElement(null, null, "sequence");
				} catch (SAXException e1) {
					e1.printStackTrace(System.err);
				}
				AttributesImpl attrs = new AttributesImpl();
				
				try {
					for (int i = 1; i != metadata.getColumnCount();i++) {
						try {
							attrs.addAttribute(null, null, null, "name",
									metadata.getColumnLabel(i));
						} catch (SQLException e1) {
							e1.printStackTrace(System.err);
						}
						try {
							th.startElement(null, null, "column", attrs);
						} catch (SAXException e1) {
							e1.printStackTrace(System.err);
						}
						try {
							try {
								th.characters(results.getString(i)
										.toCharArray(), 0, results.getString(i)
										.length());
							} catch (SAXException e) {
								e.printStackTrace(System.err);
							} catch (SQLException e) {
								e.printStackTrace(System.err);
							}
						} catch (NullPointerException e) {
							try {
								System.err.println(new Date() + ": "
										+ results.getRow() + ", " + i
										+ "th column is null");
							} catch (SQLException e1) {
								e1.printStackTrace(System.err);
							}
							try {
								th.characters("null".toCharArray(), 0, 4);
							} catch (SAXException e1) {
								e1.printStackTrace(System.err);

							}
						}
						try {
							th.endElement(null, null, "column");
						} catch (SAXException e) {
							e.printStackTrace(System.err);
						}
						attrs.clear();
					}
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
				try {
					th.endElement(null, null, "result");
				} catch (SAXException e) {
					e.printStackTrace(System.err);
				}
			} 
		} catch (SQLException e) {
			e.printStackTrace(System.err);
		}
		try {
			th.endElement(null, null, "results");
		} catch (SAXException e) {
			e.printStackTrace(System.err);
		}
		try {
			th.endElement(null, null, "qna");
		} catch (SAXException e) {
			e.printStackTrace(System.err);
		}
		try {
			th.endDocument();
		} catch (SAXException e) {
			e.printStackTrace(System.err);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace(System.err);
		}
		ret = ret_.toString();
		return ret;
	}

	public static void main(String[] args) {
		System.out.println(go());
	}
}
